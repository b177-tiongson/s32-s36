const jwt = require("jsonwebtoken")

//User defined string data thaw will be used to create JSON web tokens
const secret = "CourseBookingAPI"

//Token creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

//token verification
module.exports.verify = (req,res,next)=>{
	//the token is retrieved from the request header
	let token = req.headers.authorization

	//token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token)

		token = token.slice(7, token.length)

		//validate the token using "verify" method decrypting the token using the secret code

		return jwt.verify(token, secret, (err,data)=>{
			//if JWT is not valid
			if(err){
				return res.send({auth: "failed"})
			}
			//if valid
			else{
				//allows the application to processd with the next middleware function/calback function in the route
				next()
			}
		})
	}
	//token does not exist
	else{
		return res.send({auth:"failed"})
	}
}

//token decryption
module.exports.decode = (token)=>{
	//Token recieved and is undefined
	if(typeof token!=="undefined"){
		//Retrieves only the token and remove the "Bearer" prefix

		token = token.slice(7,token.length)

		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return null
			}
			else{

				//the "decode" method is used to obtain the information from JWT
				//the "{complete:true" option allows us to return additional information from the JWT
				// The payload contains information provided in the "createAccessToken" method
				return jwt.decode(token,{complete:true}).payload
			}
		})
	}
	//token does not exist
	else{
		return null;
	}
}