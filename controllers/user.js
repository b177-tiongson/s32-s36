const User = require("../models/User")
const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//controller function for checking email duplicates
module.exports.checkEmailExist = (reqBody)=>{
	return User.find({email: reqBody.email})
	.then(result => {
		if(result.length > 0){
			//with duplicate
			return true
		}
		else{
			//without duplicate
			return false
		}
	})

}

//controller function for user registration

module.exports.registerUser = (reqBody)=>{
	//creates variable "newUser" ans instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//save the created object to our database
	return newUser.save()
	.then((user, error)=>{
		if(error){
			return false
		}
		else{
			return true

		}
	})
}

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email})
	.then(result=>{
		//if user does no exist
		if(result.length > 0){

			return false
		}
		//user exist
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				//generate an access token
				return{access: auth.createAccessToken(result)}
			}
			//passwords do not match
			else{
				return false
			}
		}
	})
}

module.exports.getUser = (reqBody)=>{
	return User.findById(reqBody.userId)
	.then(result => {

		if(result == null){
			return false
		}
		else{
			result.password = ""
			return result	
		}

	})
}

//controller for enrolling the user to a specific course
module.exports.enroll = async(data)=>{
	//add the course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId)
	.then(user =>{
		//Add the courseId in the user's enrollments array
		user.enrollments.push({courseId: data.courseId})

		//save the updated user information in the database
		return user.save().then((user,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	})

	//add the user ID in the enrollees array of the course

	let isCourseUpdated = await Course.findById(data.courseId)
	.then(course =>{
		//adds the userId in the course's enrolees array
		course.enrolees.push({userId: data.userId})

		//save the updated course information in the database
		return course.save().then((course,error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})
	}) 


	//condition that will check if the user and course documents have been updated
	//user enrollment is successful
	if(isUserUpdated&&isCourseUpdated){
		return true
	}
	else{
		return false
	}

}
