const Course = require("../models/Course")
const auth = require("../auth")

module.exports.addCourse = (reqBody)=>{
	//create a variable "newcourse" and instantiates a new "course" object

	if(reqBody.userAdmin){

		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save()

		.then((course, error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})

	}

	else{
		return Promise.resolve(false)	
	}		
}

//get all course

module.exports.getAllCourses = () =>{
	return Course.find({})
	.then(result=>{
		return result
	})
}

//get specific course

module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId)
	.then(result=>{
		return result
	})
}

//controller function for updating a course

module.exports.updateCourse = (reqParams, reqBody) =>{
	//Specify the fields/properties of the document to be updated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error)=>{
		if(error){
			return false
		}
		else{
			return true
		}
	})
}

module.exports.archiveCourse = (reqParams, reqBody) =>{

	// if(reqBody.userAdmin){
		let isActiveStatus = {isActive: reqBody.isActive}


		return Course.findByIdAndUpdate(reqParams.courseId, isActiveStatus)
		.then((course, error)=>{
			if(error){
				return false
			}
			else{
				return true
			}
		})
	// }

	// else{
	// 	return Promise.resolve(false)
	// }
}
