const express = require("express")
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")

//route checking if the user's email already exist
//localhost/users/checkEmail
router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExist(req.body)
	.then(resultFromController => res.send(resultFromController))

})

//route for registering a user

router.post("/register",(req,res)=>{
	userController.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController))
})

//route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController))
})



//Create a /details route that will accept the user’s Id to retrieve the details of a user.

router.get("/details", auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization)

	userController.getUser({userId: userData.id})
	.then(resultFromController => res.send(resultFromController))
})


//route to enroll user to a course

router.post("/enroll",auth.verify,(req,res)=>{
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data)
	.then(resultFromController=> res.send(resultFromController))

})


module.exports = router
