const express = require("express")
const router = express.Router()
const courseController = require("../controllers/course")
const auth = require("../auth")

//route for creating a course
router.post("/",auth.verify, (req,res)=>{

	const userData = auth.decode(req.headers.authorization)


	courseController.addCourse({
		userAdmin:userData.isAdmin,
		name:req.body.name,
		description:req.body.description,
		price:req.body.price
		})

	.then(resultFromController => res.send(resultFromController))

	/*

	const data = {
			course: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));



	*/

})


// route for retrieving all the courses
router.get("/all",(req,res)=>{
	courseController.getAllCourses()
	.then(resultFromController=>res.send(resultFromController))
})

//route for retrieving specific course
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId)

	courseController.getCourse(req.params)
	.then(resultFromController => res.send(resultFromController))
})

//route for updating a course
router.put("/:courseId", auth.verify, (req,res)=>{
	console.log(req.params.courseId)

	courseController.updateCourse(req.params,req.body)
	.then(resultFromController => res.send(resultFromController))
})

//archive

router.put("/:courseId/archive",auth.verify, (req,res)=>{
	console.log(req.params.courseId)

	let data = {
		userId: auth.decode(req.body.userId).id,
		courseId: req.body.courseId
	}


	courseController.archiveCourse(req.params,req.body)
	.then(resultFromController => res.send(resultFromController))

})





module.exports = router